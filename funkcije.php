<?php
	function redirekcija_ka($nova_lokacija) {
		header("Location: " . $nova_lokacija);
		exit;
	}
        
        
	// Prisutnost
	function prisutnost_vrednosti($vrednost) {		
			return isset($vrednost) && $vrednost !== "";
	}
	// Duzina stringa

	function max_broj_karaktera($vrednost, $max) {			
			return strlen($vrednost) <= $max ;
	}
	// Ukljuceno u set
			
	function ukljuceno_u_set($vrednost, $set) {			
			return in_array($vrednost, $set) ;
	}
	// Proveravanje max dizine unete vrednosti u polje 
	function proveri_max_duzinu ($polje_sa_max_duzinom) {
	global $greske;		
		foreach($polje_sa_max_duzinom as $polje => $max) {
			$vrednost = trim($_POST[$polje]);
			if (!max_broj_karaktera($vrednost, $max)){
				$greske[$polje] = ucfirst($polje). " je predugacak";
			}
		}
	}

	function prikaz_greske($greske = array())	{
		$output = "";
		if (!empty($greske)) {
		$output .= "<div class=\"greska\">";
		$output .= "Molimo ispravite sledece podatke:";
		$output .= "<ul>";
		
			foreach ($greske as $naziv => $greska) {
				$output .= "<li>{$greska}</li>";
				
			}
		
		$output .= "</ul>";
		$output .= "</div>";
	}

		return $output;
	}
?>
