<?php include('konekcija.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Please register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="author" content="">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Google Font's -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lustria' rel='stylesheet' type='text/css'>
        <style>
            .error {color: #ff0000;}
            h1{
                text-align: center;
                margin-bottom: 100px;
            }
            .container{
                position: relative;
            }
        </style>
    </head>

    <body>
        <?php
// define variables and set to empty values
        $nameErr = $passwordErr = $emailErr = $genderErr = $websiteErr = $humanErr = $commentErr = "";
        $name = $password = $email = $gender = $comment = $website = $human = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["username"])) {
                $nameErr = "Name is required";
            } else {
                $name = test_input($_POST["username"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                    $nameErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST["password"])) {
                $passwordErr = "Name is required";
            } else {
                $password = test_input($_POST["password"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $password)) {
                    $passwordErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
            } else {
                $email = test_input($_POST["email"]);
                // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }

            if (empty($_POST["website"])) {
                $website = "";
            } else {
                $website = test_input($_POST["website"]);
                // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
                if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $website)) {
                    $websiteErr = "Invalid URL";
                }
            }

            if (empty($_POST["comment"])) {
                $comment = "";
            } else {
                $comment = test_input($_POST["comment"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $comment)) {
                    $commentErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["human"])) {
                $humanErr = "Result is required";
            } else {
                $human = test_input($_POST["human"]);
                // check if human only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $human)) {
                    $humanErr = "Only letters and white space allowed";
                } else {
                    $human = intval($_POST['human']);
                    while ($human != 5) {
                        echo 'Your result is incorrect!';
                    }
                }
            }

            if (empty($_POST["gender"])) {
                $genderErr = "Gender is required";
            } else {
                $gender = test_input($_POST["gender"]);
            }


            $query = "INSERT INTO users (";
            $query .= "  username, password";
            $query .= ") VALUES (";
            $query .= "  '{$name}', '{$password}'";
            $query .= ")";
//            $rezultat = mysqli_query($conn, $query);

            

            if (mysqli_query($conn, $query)) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $query . "<br>" . mysqli_error($conn);
            }

            mysqli_close($conn);
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        ?>
        <div class="container">
            <h1>Please Register</h1>
            <p><span class="error">* required field.</span></p>
            <form class="form-horizontal" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $name; ?>">
                        <span class="error">* <?php echo $nameErr; ?></span>
                        <?php
                        if (isset($_POST["submit"])) {
                            if (strlen($name) < 2) {
                                echo "Please enter 2 caracters as minimum.";
                            } elseif (strlen($name) > 20) {
                                echo "Please enter less than 20 caracters.";
                            }
                        }
                        ?>
                        <?php // echo "<p class='text-danger'>$errName</p>"; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="password" placeholder="Password" value="<?php echo $password; ?>">
                        <span class="error">* <?php echo $passwordErr; ?></span>
                        <?php
                        if (isset($_POST["submit"])) {
                            if (strlen($name) < 2) {
                                echo "Please enter 2 caracters as minimum.";
                            } elseif (strlen($name) > 20) {
                                echo "Please enter less than 20 caracters.";
                            }
                        }
                        ?>
                        <?php // echo "<p class='text-danger'>$errName</p>"; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo $email; ?>">
                        <span class="error">* <?php echo $emailErr; ?></span>
                        <?php //echo "<p class='text-danger'>$errEmail</p>";    ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="website" class="col-sm-2 control-label">Website</label>
                    <div class="col-sm-10">
                        <input type="website" class="form-control" id="website" name="website" placeholder="www.something.com" value="<?php echo $website; ?>">
                        <span class="error"><?php echo $websiteErr; ?></span>
                        <?php //echo "<p class='text-danger'>$errEmail</p>";    ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="col-sm-2 control-label">Comment</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="4" name="comment"><?php echo $comment; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="human" name="human" placeholder="Your Answer" value="<?php echo $human; ?>">
                        <?php
                        if (isset($_POST["submit"])) {
                            if ($human != 5) {
                                echo 'Your result is incorrect!';
                            }
                        }
                        ?>
                    </div>
                </div>


                Gender:
                <input type="radio" name="gender" <?php if (isset($gender) && $gender == "female") echo "checked"; ?>  value="female">Female
                <input type="radio" name="gender" <?php if (isset($gender) && $gender == "male") echo "checked"; ?>  value="male">Male
                <span class="error">* <?php echo $genderErr; ?></span>
                <br><br>

                <div class="form-group1">
                    <div class="col-sm-10 col-sm-offset-2">
                        <input style="float:right; width: 200px;" id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>

        <!-- Start JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- / JavaScript -->
    </body>
</html>
