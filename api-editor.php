<?php

//kreirati funkciju da li si ulogovan


function obrisi_knjigu() {
    return TRUE; //true rezultat vraca kada je uspesno izvrseno brisanje
    //return FALSE;
}

function dodaj_knjigu() {
    return TRUE; //true rezultat vraca kada je uspesno izvrseno dodavanje
    //return FALSE;
}

function izmeni_knjigu() {
    return TRUE; //true rezultat vraca kada je uspesno izvrseno izmena
    //return FALSE;
}

//segment
//$knjige = array_values(array($knjiaga1, $knjiga2));
//$output['knjige'] = $knjige;
$output['page'] = '';
$output['success'] = FALSE;
$output['error'] = ""; // error code
$output['msg'] = "";


$novaKnjiga = array(//podaci dolaze sa post metodom koji ih smesta iz forme u bazu
    "naziv" => "Nuklearna fizika",
    "autor" => "Zoran",
    "godina izdavanja" => "2017",
    "jezik" => "Engleski",
    "originalni jezik" => "Srpski"
);
$izmenjenaKnjiga = array(//u ovu promenljivu idu podaci koje korisnik submituje kako bi izmenio karakteristike
    "naziv" => "Istorija",
    "autor" => "Milica",
    "godina izdavanja" => "2011",
    "jezik" => "Engleski",
    "originalni jezik" => "Srpski"
);


//Kreiranje, brisanje i ažuriranje knjiga je moguće samo ulogovanim korisnicima.
$ulogovan = true;

if ($ulogovan) {
//$_GET obavezno prepraviti u $_POST
    if (isset($_GET['action'])) {

        if ($_GET['action'] == 'create') {
            if (dodaj_knjigu()) {
                $output['knjige'] = array_values(array($novaKnjiga));
                $output['success'] = TRUE;
                $output['msg'] = "The book is the successfully created";
            } else {
                unset($output['knjige']);
                $output['success'] = FALSE;
                $output['error'] = "CREATING_FAILED"; // error code
                $output['msg'] = "Something went wrong creating feild";
            }
        } elseif ($_GET['action'] == 'update') {
            if (izmeni_knjigu()) {
                $output['knjige'] = array_values(array($izmenjenaKnjiga));
                $output['success'] = TRUE;
                $output['msg'] = "The book is the successfully updated";
            } else {
                unset($output['knjige']);
                $output['success'] = FALSE;
                $output['error'] = "UPDATING_FAILED"; // error code
                $output['msg'] = "Something went wrong updating feild";
            }
        } elseif ($_GET['action'] == 'delete') {
            if (obrisi_knjigu()) {
                unset($output['knjige']);
                $output['success'] = TRUE;
                $output['msg'] = "The book is the successfully deleted";
            } else {
                unset($output['knjige']);
                $output['success'] = FALSE;
                $output['error'] = "DELETING_FAILED"; // error code
                $output['msg'] = "Something went wrong deleting feild";
            }
        } else {
            unset($output['knjige']);
            $output['success'] = FALSE;
            $output['error'] = "UNDEFINED_ACTION"; // error code
            $output['msg'] = "Nedostaje action";
        }
    } else {
        unset($output['knjige']);
        $output['success'] = FALSE;
        $output['error'] = "UNDEFINED_ACTION"; // error code
        $output['msg'] = "Nedostaje action";
    }
} else {
    unset($output['knjige']);
    $output['success'] = FALSE;
    $output['error'] = "NEED_LOGIN"; // error code
    $output['msg'] = "Morate biti ulogovani da bise koristili ovaj api";
}

$output_original = json_encode($output, JSON_PRETTY_PRINT);
echo '<pre>';
echo $output_original;
echo '<pre>';
