<?php

function pretrazi_knjige($query) {
    $knjiga3 = array(array(
            "naziv" => "Put oko sveta",
            "autor" => "Jules Verne",
            "godina izdavanZil Vernja" => "1900",
            "jezik" => "Srpski",
            "originalni jezik" => "Francuski"
        )
    );

    return $knjiga3;
}

function izlistaj_knjige() {
    $knjiaga1 = array(
        "naziv" => "Anatomija",
        "autor" => "Jon",
        "godina izdavanja" => "2007",
        "jezik" => "Engleski",
        "originalni jezik" => "Engleski"
    );
    $knjiga2 = array(
        "naziv" => "Biologija",
        "autor" => "Nenad",
        "godina izdavanja" => "2014",
        "jezik" => "Engleski",
        "originalni jezik" => "Engleski"
    );
    $knjige = array_values(array($knjiaga1, $knjiga2));
    return $knjige;
}

//segment
$output['knjige'] = array();
$output['page'] = '';
$output['success'] = FALSE;
$output['error'] = ""; // error code
$output['msg'] = "";


if (isset($_GET['action'])) {
    if ($_GET['action'] == 'izlistaj') {
        //paginacija http://localhost/knjige/api.php?action=izlistaj&page=1

        $sve_knjige = izlistaj_knjige();


        $page = '';
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $broj_knjiga = count($sve_knjige);

        if ($broj_knjiga > ((intval($page) - 1) * 10)) {
            //$output['knjige'] = array_values(array($knjiaga1, $knjiga2));
            $rez1 = (($page - 1) * 10);
            //$output['knjige'] = array_slice($knjige, $rez1, 10);

            $output['knjige'] = array_slice($sve_knjige, $rez1, 10);
        } else {
            $output['page'] = '';
            $output['knjige'] = array();
        }
        $output['success'] = TRUE;
        unset($output['error']);
    } elseif ($_GET['action'] == 'pretrazi') {
        $query = '';
        if (isset($_GET['q'])) {
            $query = $_GET['q'];
        }
        $output['knjige'] = pretrazi_knjige($query);
        unset($output['page']);
        $output['success'] = TRUE;
        $output['custom dodatak'] = 'bla bla';
        unset($output['error']);
    } else {
        $output['knjige'] = null;
        $output['success'] = FALSE;
        $output['error'] = "UNDEFINED_ACTION"; // error code
        $output['msg'] = "Nedostaje action";
    }
} else {
    $output['knjige'] = null;
    $output['success'] = FALSE;
    $output['error'] = "UNDEFINED_ACTION"; // error code
    $output['msg'] = "Nedostaje action";
}

//zavrsna faza output podataka
$output_json = json_encode($output, JSON_PRETTY_PRINT);
$output_json_array = json_encode(array_values($output), JSON_PRETTY_PRINT);


echo '<pre>';
echo $output_json;


