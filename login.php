<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Please register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="author" content="">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Google Font's -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lustria' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <?php
        require_once("funkcije.php");

        $greske = array();
        $poruka = "";

        if (isset($_POST["submit"])) {
            //Forma je submitovana
            $korisnik = trim($_POST["name"]);
            $lozinka = trim($_POST["email"]);
            // Provera	
            $neophodna_polja = array("name", "email");

            foreach ($neophodna_polja as $polje) {
                $vrednost = trim($_POST[$polje]);
                if (!prisutnost_vrednosti($vrednost)) {
                    $greske[$polje] = ucfirst($polje) . " ne moze biti prazno";
                }
            }


            // PREBACENO U FUNKCIJE!
            // Koriscenje asoc. niza
            $polje_sa_max_duzinom = array("name" => 10, "email" => 30);
            /*
              foreach($polje_sa_max_duzinom as $polje => $max) {
              $vrednost = trim($_POST[$polje]);
              if (!max_broj_karaktera($vrednost, $max)){
              $greske[$polje] = ucfirst($polje). " je predugacak";
              }
              } */
            proveri_max_duzinu($polje_sa_max_duzinom);

            if (empty($greske)) {
                // Proba za logovanje
                if ($korisnik == "admin" && $lozinka == "admin@admin.com") {
                    //Uspesno logovanje
                    redirekcija_ka("rest/index.php");
                } else {
                    $poruka = "Korisnicko ime ili lozinka se ne podudaraju.";
                }
            }
        } else {
            $korisnik = "";
            $poruka = "Molimo ulogujte se.";
        }
        ?>

        <?php echo $poruka; ?>

        <?php echo prikaz_greske($greske); ?>
        <div style="margin-top: 100px;" class="container">
            <form class="form-horizontal" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="Name" value="<?php echo htmlspecialchars($korisnik); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" placeholder="Email" value="">
                    </div>
                </div>
                <br />
                <input type="submit" name="submit" value="Send"/>
            </form>
        </div>


        <!-- Start JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- / JavaScript -->
    </body>
</html>
