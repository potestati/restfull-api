<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="author" content="">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Google Font's -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lustria' rel='stylesheet' type='text/css'>
        <style>
            a {
                padding: 20px 0;
                width: 200px;
                background: orange;
                margin-top: 100px;
                color: black;
            }
            .container{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Welcome</h1>
            <a style="float:left" class="btn" href="registration.php">Register</a>
            <a style="float:right" class="btn" href="login.php">Login</a>
        </div>
        <!-- Start JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- / JavaScript -->
        <!-- Main JS -->
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
